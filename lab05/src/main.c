#include <stdio.h>

int main()
{
	int a, b, max, min, r, result;
	if (a > b) { //визначаемо найбільше число
		max = a;
		min = b;
	} else {
		max = b;
		min = a;
	}
	//r = max % min; // формула розрахунку НСД
	do {
		r = max % min;
		max = min;
		min = r;
	} while (r != 0);
	if (max != 1) {
		result = max;
	} else {
		result = 1;
	}
	return 0;
}

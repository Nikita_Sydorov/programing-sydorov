# Модуль 2. Основи програмування на мові С. Лабораторна робота №5. Циклічні конструкції

Мета: розробити програму завдання №22

## 1 ВИМОГИ

### 1.1 Розробник

Інформація про розробника

- Сидоров Н. І.;
- КІТ-121б;
- Взавдання №19.

### 1.2 Загальне завдвння

1) Виконати одне завдання з пулу завдань на свій розсуд згідно її складності.

### 1.3 Задача

1. Створити репозиторий для лабораторнної роботи. └── lab05 

   																									├── Makefile 
	
   																									├── README.md 
	
   																									├── doc 
	
   																													 └── lab05.txt 
	
   																									└── src 
	
   																													└── main.c

2. Створити програму яка визначає найбільший спільний дільник для двох заданих чисел

3. Запуск програми.

4. Зупинка посередині виконання програми за допомогою breakpoints.

## 2 ОПИС ПРОГРАМИ

### 2.1 Функціональне призначення

 Программа визначає найбільший спільний дільник для двох заданих чисел

### **2.2 Опис логічної структури**

1. підключення бібліотеки **stdio.h**

2. початок- точка входу **int main()**

3. визначення типу змінних

4. Алгоритм програми:

   1. вводимо змінні
   2. визначаемо, яке з чисел більше
   3. за формулою НСД здійснюємо розрахунок за допомогою циклу **while**
   4. виводимо результат
   5. компіляція файлу
   6. відкриття файлу у відлагоднику


### **2.3 Важливі фрагменти програми**

1. Код порграми для WHILE
```c
int main()
{ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
	int a, b, max, min, r, result;
	if (a > b) { //визначаемо найбільше число
		max = a;
		min = b;
	} else {
		max = b;
		min = a;
	}
	r = max % min; // формула розрахунку НСД
	while (r != 0) { // цикл, у якому за вказаною формулою розраховується НСД
		r = max % min;
		max = min;
		min = r;
	}
	if (max != 1) {
		result = max;
	} else {
		result = 1;
	}
	return 0;
}
```
2. Компіляція файлу

   `make run`

3. відкриття файлу у відлагоднику **lldb**

   `lldb main.bin`

   ```c
   (lldb) target create "main.bin"
   Current executable set to '/home/pax-linux/lab05/dist/main.bin' (x86_64).
   (lldb) b main.c:3
   Breakpoint 1: where = main.bin`main + 11 at main.c:4:4, address = 0x000000000040111b
   (lldb) r
   Process 3686 launched: '/home/pax-linux/lab05/dist/main.bin' (x86_64)
   Process 3686 stopped
   * thread #1, name = 'main.bin', stop reason = breakpoint 1.1
       frame #0: 0x000000000040111b main.bin`main at main.c:4:4
   (lldb) p a=240
   (int) $0 = 240
   (lldb) p b=126
   (int) $1 = 126
   Далі виконуемо програму й отримуємо
   (lldb) p result
   (int) $2 = 6
   Отже, НСД=6
   ```

1. Код програми для FOR

   ```c
   int main()
   {
   	int a, b, max, min, r, result;
   	if (a > b) { //визначаемо найбільше число
   		max = a;
   		min = b;
   	} else {
   		max = b;
   		min = a;
   	}
   	r = max % min; // формула розрахунку НСД
   	for (int i; r != 0; i++) {
   		r = max % min;
   		max = min;
   		min = r;
   	}
   	if (max != 1) {
   		result = max;
   	} else {
   		result = 1;
   	}
   	return 0;
   }```

5. Компіляція файлу

   `make run`

6. відкриття файлу у відлагоднику **lldb**

   `lldb main.bin`

7. ```c
    (lldb) target create "main.bin"
    Current executable set to '/home/pax-linux/lab05/dist/main.bin' (x86_64).
    (lldb) b main.c:2
    Breakpoint 1: where = main.bin`main + 11 at main.c:4:6, address = 0x000000000040111b
    /*Вводимо числа*/
    (lldb) p a=240
    (int) $0 = 240
    (lldb) p b=126
    (int) $1 = 126
    /*Виконуємо програму й отримуємо*/
    (lldb) p result 
    (int) $2 = 6

8. Код програми для DO-WHILE

9. ```c
   #include <stdio.h>
   
   int main()
   {
   	int a, b, max, min, r, result;
   	if (a > b) { //визначаемо найбільше число
   		max = a;
   		min = b;
   	} else {
   		max = b;
   		min = a;
   	}
   	//r = max % min; // формула розрахунку НСД
   	do {
   		r = max % min;
   		max = min;
   		min = r;
   	} while (r != 0);
   	if (max != 1) {
   		result = max;
   	} else {
   		result = 1;
   	}
   	return 0;
   }
   ```

10. Компіляція файлу

    `make run`

11. відкриття файлу у відлагоднику **lldb**

    `lldb main.bin`

12. ```c
    (lldb) target create "main.bin"
    Current executable set to '/home/pax-linux/lab05/dist/main.bin' (x86_64).
    (lldb) b main.c:2
    Breakpoint 1: where = main.bin`main + 11 at main.c:6:6, address = 0x000000000040111b
    /*Вводимо числа*/
    (lldb) p a=240
    (int) $0 = 240
    (lldb) p b=126
    (int) $1 = 126
    /*Виконуємо програму й отримуємо*/
    (lldb) p result 
    (int) $2 = 6
    ```

## 3 ВАРІАНТИ ВИКОРИСТАННЯ

Програму можна використовувати для розрахунку НСД двох чисел.

## ВИСНОВОКИ

Розробив програму яка розраховує НСД двох чисел.
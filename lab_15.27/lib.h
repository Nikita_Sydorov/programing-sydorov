#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Mail {
	char draft[10];
	char sender[50];
	char recipient[50];
	char theme[100];
	char text[100];
	char encoding[10];
};

void get_from_file(char *file_name);

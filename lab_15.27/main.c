#include "lib.h"

int main(int argc, char **argv) {
	/* Створення змінних, що зберігають у собі дирректорії з вхідним файлом та вихідним файлом */
	char *input_dir = *(argv + 1), *output_dir = *(argv + 2);

	/* Читання елементів з файлу */
	get_from_file(input_dir);

	return 0;
}

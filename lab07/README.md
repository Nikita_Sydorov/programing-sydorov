# Модуль 2. Основи програмування на мові С. Лабораторна робота №7. Функції

Мета: розробити програму завдання №22

## 1 ВИМОГИ

### 1.1 Розробник

Інформація про розробника

- Сидоров Н. І.;
- КІТ-121б;
- Взавдання №19.

### 1.2 Загальне завдвння

1) Виконати одне завдання з пулу завдань на свій розсуд згідно її складності.

### 1.3 Задача

1. Створити репозиторий для лабораторнної роботи. └── lab07 

   																									├── Makefile 
		
   																									├── README.md 
		
   																									├── doc 
		
   																													 └── lab07.txt 
		
   																									└── src 
		
   																													└── main.c

2. Переробити програми, що були розроблені під час виконання лабораторних робіт з тем “Масиви” та “Цикли” таким чином, щоб використовувалися функції для обчислення результату. Функції повинні задовольняти основну їх причетність - уникати дублювання коду. Тому, для демонстрації роботи, ваша програма (функція main()) повинна мати можливість викликати розроблену функцію з різними вхідними даними.

3. Запуск програми.

4. Зупинка посередині виконання програми за допомогою breakpoints.

## 2 ОПИС ПРОГРАМИ

### 2.1 Функціональне призначення

 Розробка функцій для попередніх лабораторних робіт

### **2.2 Опис логічної структури**

1. Алгоритм програми:
   1. створюемо функцію
   2. викликаємо її в main
   3. компіляція файлу
   4. відкриття файлу у відлагоднику



### **2.3 Важливі фрагменти програми**

1. Код порграми для WHILE

```c
int ncd(int max, int min){
    int r=0, result=0;
    r = max % min; // формула розрахунку НСД
	while (r != 0) { // цикл, у якому за вказаною формулою розраховується НСД
		r = max % min;
		max = min;
		min = r;
	}
	if (max != 1) {
		result = max;
	} else {
		result = 1;
	}
	return result;
}

int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
	int a, b, max, min, r, result;
	if (a > b) { //визначаемо найбільше число
		max = a;
		min = b;
	} else {
		max = b;
		min = a;
	}
	result = ncd(max, min);
}
```

2. Компіляція файлу

   `make run`

3. відкриття файлу у відлагоднику **lldb**

   `lldb main.bin`

   ```c
   (lldb) target create "main.bin"
   Current executable set to '/home/pax-linux/FOR_LAB/lab07/dist/main.bin' (x86_64).
   (lldb) b main.c:19
   Breakpoint 1: where = main.bin`main + 15 at main.c:19:6, address = 0x000000000040118f
   (lldb) r
   Process 7392 launched: '/home/pax-linux/FOR_LAB/lab07/dist/main.bin' (x86_64)
   Process 7392 stopped
   * thread #1, name = 'main.bin', stop reason = breakpoint 1.1
       frame #0: 0x000000000040118f main.bin`main at main.c:19:6
      16  	
      17  	int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
      18  		int a, b, max, min, r, result;
   -> 19  		if (a > b) { //визначаемо найбільше число
      20  			max = a;
      21  			min = b;
      22  		} else {
   (lldb) 
   /*Вводимо числа*/
   (lldb) p a=240
   (int) $0 = 240
   (lldb) p b=126
   (int) $1 = 126
   Далі виконуемо програму й отримуємо
      26  		result = ncd(max, min);
   -> 27  	}
   (lldb) p result
   (int) $3 = 6
   ```

4. Код програми для FOR

   ```c
   int ncd(int max, int min){
       int r=0, result=0;
       r = max % min; // формула розрахунку НСД
   	for (int i; r != 0; i++) {
   		r = max % min;
   		max = min;
   		min = r;
   	}
   	if (max != 1) {
   		result = max;
   	} else {
   		result = 1;
   	}
   	return result;
   }
   
   int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
   	int a, b, max, min, r, result;
   	if (a > b) { //визначаемо найбільше число
   		max = a;
   		min = b;
   	} else {
   		max = b;
   		min = a;
   	}
   	result = ncd(max, min);
   }
   ```

5. Компіляція файлу

   `make run`

6. відкриття файлу у відлагоднику **lldb**

   `lldb main.bin`

7. ```c
   (lldb) target create "main.bin"
   Current executable set to '/home/pax-linux/FOR_LAB/lab07/dist/main.bin' (x86_64).
   (lldb) b main.c:19
   Breakpoint 1: where = main.bin`main + 15 at main.c:19:6, address = 0x000000000040119f
   (lldb) r
   Process 7561 launched: '/home/pax-linux/FOR_LAB/lab07/dist/main.bin' (x86_64)
   Process 7561 stopped
   * thread #1, name = 'main.bin', stop reason = breakpoint 1.1
       frame #0: 0x000000000040119f main.bin`main at main.c:19:6
      16  	
      17  	int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
      18  		int a, b, max, min, r, result;
   -> 19  		if (a > b) { //визначаемо найбільше число
      20  			max = a;
      21  			min = b;
      22  		} else {
   (lldb) 
   
   /*Вводимо числа*/
   (lldb) p a=240
   (int) $0 = 240
   (lldb) p b=126
   (int) $1 = 126
   /*Виконуємо програму й отримуємо*/
   Process 7561 stopped
   * thread #1, name = 'main.bin', stop reason = step over
       frame #0: 0x00000000004011d6 main.bin`main at main.c:27:1
      24  			min = a;
      25  		}
      26  		result = ncd(max, min);
   -> 27  	}
   (lldb) p result
   (int) $3 = 6
   ```

8. Код програми для DO-WHILE

9. ```c
   int ncd(int max, int min){
       int r=0, result=0;
       do {
   		r = max % min;
   		max = min;
   		min = r;
   	} while (r != 0);
   	if (max != 1) {
   		result = max;
   	} else {
   		result = 1;
   	}
   	return result;
   }
   
   int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
   	int a, b, max, min, r, result;
   	if (a > b) { //визначаемо найбільше число
   		max = a;
   		min = b;
   	} else {
   		max = b;
   		min = a;
   	}
   	result = ncd(max, min);
   }
   ```

10. Компіляція файлу

    `make run`

11. відкриття файлу у відлагоднику **lldb**

    `lldb main.bin`

12. ```c
    (lldb) target create "main.bin"
    Current executable set to '/home/pax-linux/FOR_LAB/lab07/dist/main.bin' (x86_64).
    (lldb) b main.c:18
    Breakpoint 1: where = main.bin`main + 15 at main.c:18:6, address = 0x000000000040117f
    (lldb) r
    Process 7720 launched: '/home/pax-linux/FOR_LAB/lab07/dist/main.bin' (x86_64)
    Process 7720 stopped
    * thread #1, name = 'main.bin', stop reason = breakpoint 1.1
        frame #0: 0x000000000040117f main.bin`main at main.c:18:6
       15  	
       16  	int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
       17  		int a, b, max, min, r, result;
    -> 18  		if (a > b) { //визначаемо найбільше число
       19  			max = a;
       20  			min = b;
       21  		} else {
    (lldb) 
    /*Вводимо числа*/
    (lldb) p a=240
    (int) $0 = 240
    (lldb) p b=126
    (int) $1 = 126
    /*Виконуємо програму й отримуємо*/
       25  		result = ncd(max, min);
    -> 26  	}
    (lldb) p result 
    (int) $2 = 6
    ```

## 3 ВАРІАНТИ ВИКОРИСТАННЯ

Можна використовувати функції для розрахунку НСД двох чисел(за допомогою циклів For, While, Do-while).

## ВИСНОВОКИ

Розробив функції, для розрахунку НСД двох чисел(за допомогою циклів For, While, Do-while).
int ncd(int max, int min){
    int r=0, result=0;
    do {
		r = max % min;
		max = min;
		min = r;
	} while (r != 0);
	if (max != 1) {
		result = max;
	} else {
		result = 1;
	}
	return result;
}

int main(){ /*22  Визначити найбільший спільний дільник для двох заданих чисел*/
	int a, b, max, min, r, result;
	if (a > b) { //визначаемо найбільше число
		max = a;
		min = b;
	} else {
		max = b;
		min = a;
	}
	result = ncd(max, min);
}

# Модуль 1. Вступ до прграмування. Лабораторна робота №2. Вступ до програмування. Основи debug процессу

Мета: Ознайомитися з основами debug процесу.

## 1 ВИМОГИ

### 1.1 Розробник

Інформація про розробника

- Сидоров Н. І.;
- КІТ-121б;
- Дана лабораторна робота не передбачає розподіл на варіанти.

### 1.2 Загальне завдвння

1) Освоєння debag процесу;
2) Дана лабораторна спрямована на установку середовища для подальшої роботі з предмету “Програмування”. Тому, ніякий код писати не треба.

### 1.3 Задача

1. Зареєструватися (якщо треба) на однієї з загально-доступних систем (рекомендовано), таких як **gitlab**, **github**.

2. Склонуватися з створеного репозиторія.

3. Скопіювати наступні файли (з урахуванням каталогів) з зовнішнього репозиторія (директорії **lab00**), що був наданий у попередній роботі у під-директорію **lab02**: 

   • .clang-format

   • .clang-tidy 

   • .gitlab-ci.yml

   • lab00/Doxyfile

   • lab00/Makefile

   • lab00/README.md

   • lab00/каталог “src”

   • lab00/каталог “test”

4. Зафіксувати зміни (за допомогою команди *git commit*) під назвою “**Initial copy of sam-ple_project**”.

5. Виконати зміни, зібрати проект (зкомпілувати) та продемонструвати зміни.

6. Відкрити в відлагоднику **(debugger) lldb** ваш виконуючий файл, зупинитися на будь-якій строці та визначити значення змінних (variables).

7. Зафіксувати зміни за допомогою команди *git commit*.

## 2 ОПИС ПРОГРАМИ

1. Зареєструвався на **githab** programing-Sydorov.

2. Клонував данні з створенного репозиторію.

3. Скопіював настпуні файли до **Lab_2/programing-Sydorov/lab02**: **.clang-format**, **.clang-tidy**, **.gitlab-ci.yml**, **lab00/Doxyfile**, **lab00/Makefile**, **lab00/README.md**, **lab00/каталог “src”**, **lab00/каталог “test”**.

4. Зафіксував зміни за допомогою соманди *git commit*. Для цього необхідно було виконати коамнду g*it add lab02* для директорії **lab02**(так було вказано зробити після вводу команди *git commit*).

5. Прокомпілювава проєкт(*make all*)

6. Інсталював додаток **lldb**: *sudo apt install lldb*.

7. запустив відлагодник *lldb main.bin*. 

8. ```assembly
   (lldb) target create "main.bin"
   Current executable set to '/home/pax-linux/lab02/programing-sydorov/lab02/dist/main.bin' (x86_64).
   (lldb) b main.c:40
   Breakpoint 1: where = main.bin`main + 22 at main.c:40:22, address = 0x0000000000401316
   (lldb) r
   Process 6406 launched: '/home/pax-linux/lab02/programing-sydorov/lab02/dist/main.bin' (x86_64)
   Process 6406 stopped
   * thread #1, name = 'main.bin', stop reason = breakpoint 1.1
       frame #0: 0x0000000000401316 main.bin`main at main.c:40:22
      37  	 */
      38  	int main()
      39  	{
   -> 40  		srand((unsigned int)time(0));
      41  		struct animal animals[ANIMAL_COUNT];
      42  	
      43  		for (unsigned int i = 0; i < ANIMAL_COUNT; i++) {
   (lldb) 
   /*Вивів значення деякої змінної*/
   (lldb) p animals
   (animal [20]) $0 = {
     [0] = (type = PIG, height = 0, weight = 0)
     [1] = (type = PIG, height = 0, weight = 0)
     [2] = (type = PIG, height = 0, weight = 0)
     [3] = (type = PIG, height = 0, weight = 0)
     [4] = (type = PIG, height = 0, weight = 0)
     [5] = (type = PIG, height = 0, weight = 0)
     [6] = (type = PIG, height = 0, weight = 0)
     [7] = (type = PIG, height = 0, weight = 0)
     [8] = (type = PIG, height = 0, weight = 4294963126)
     [9] = (type = CAT | MAN | 0x7ff8, height = 0, weight = 0)
     [10] = (type = PIG, height = 0, weight = 0)
     [11] = (type = PIG, height = 0, weight = 0)
     [12] = (type = PIG, height = 0, weight = 0)
     [13] = (type = PIG, height = 4194368, weight = 0)
     [14] = (type = CAT | 0x8, height = 0, weight = 4294958608)
     [15] = (type = CAT | MAN | 0x7ff8, height = 4294959657, weight = 32767)
     [16] = (type = 0xf7fd15e0, height = 32767, weight = 4199389)
     [17] = (type = PIG, height = 4160438216, weight = 32767)
     [18] = (type = 0x401390, height = 0, weight = 0)
     [19] = (type = PIG, height = 4198512, weight = 0)
   }
   ```

9. Завантажив репозиторій на Gitlab

   ``` assembly
   pax-linux@paxlinux-VirtualBox:~/lab02/programing-sydorov$ git push
   Username for 'https://gitlab.com': Raxrino@gmail.com
   Password for 'https://Raxrino@gmail.com@gitlab.com': 
   Перечисление объектов: 20, готово.
   Подсчет объектов: 100% (20/20), готово.
   При сжатии изменений используется до 2 потоков
   Сжатие объектов: 100% (18/18), готово.
   Запись объектов: 100% (18/18), 12.32 КиБ | 4.11 МиБ/с, готово.
   Всего 18 (изменения 2), повторно использовано 0 (изменения 0)
   To https://gitlab.com/Nikita_Sydorov/programing-sydorov.git
      752de44..1e45b25  main -> main
   pax-linux@paxlinux-VirtualBox:~/lab02/programing-sydorov$ 
   ```

   

## 3 ВАРІАНТИ ВИКОРИСТАННЯ

1) Використовувати **Gitlab** для подальшої роботі з предмету “**Програмування**”.

## ВИСНОВОКИ

Ознайомилися з основами debug процесу.
